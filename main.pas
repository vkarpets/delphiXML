unit main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, MSXML2_TLB, xmldom, Xmlxform, Spin, ExtCtrls, ActnList,
  OleCtrls, SHDocVw, ComCtrls, XMLIntf, msxmldom, MSHTML;

type
  TfrmMain = class(TForm)
    acMain: TActionList;
    acMoveUp: TAction;
    acMoveDown: TAction;
    GroupBox2: TGroupBox;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label3: TLabel;
    edtArraySize: TSpinEdit;
    edtRange: TSpinEdit;
    btnMoveUp: TButton;
    btnMoveDown: TButton;
    Button1: TButton;
    Button2: TButton;
    lbArrayList: TListBox;
    acClear: TAction;
    acRemove: TAction;
    btnGenerate: TButton;
    GroupBox3: TGroupBox;
    Label2: TLabel;
    edtSearchValue: TEdit;
    btnSearch: TButton;
    Button3: TButton;
    PageControl1: TPageControl;
    tsXML: TTabSheet;
    memoXML: TRichEdit;
    tsXSLT: TTabSheet;
    tsWebBrowser: TTabSheet;
    memoXSLT: TRichEdit;
    wbBrowser: TWebBrowser;
    GroupBox4: TGroupBox;
    edtXSLT: TEdit;
    btnXSLT: TButton;
    OpenDialog: TOpenDialog;
    GroupBox5: TGroupBox;
    btnViewInBrowser: TButton;
    procedure btnGenerateClick(Sender: TObject);
    procedure acMoveUpExecute(Sender: TObject);
    procedure acMoveDownExecute(Sender: TObject);
    procedure acMoveUpUpdate(Sender: TObject);
    procedure acMoveDownUpdate(Sender: TObject);
    procedure acClearExecute(Sender: TObject);
    procedure acClearUpdate(Sender: TObject);
    procedure acRemoveExecute(Sender: TObject);
    procedure acRemoveUpdate(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnXSLTClick(Sender: TObject);
    procedure btnViewInBrowserClick(Sender: TObject);
  private
    { Private declarations }
    XMLDoc: IXMLDOMDocument;
    XSLTDoc: IXMLDOMDocument;
  public
    { Public declarations }
  end;

const
  XSLTFilter = 'XSLT files (*.xslt)|*.xslt|All files (*.*)|*.*';

var
  frmMain: TfrmMain;

implementation

uses
  ucommon;

{$R *.dfm}

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  XMLDoc := CoDOMDocument.Create;
  XSLTDoc := CoDOMDocument.Create;
  memoXML.Lines.Clear;
  memoXSLT.Lines.Clear;
  OpenDialog.InitialDir := ExtractFilePath(Application.ExeName);
  wbBrowser.Navigate('about:blank');
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  XMLDoc  := nil;
  XSLTDoc := nil;
end;

procedure TfrmMain.btnGenerateClick(Sender: TObject);
var
  elementCount : Integer;
  range : Integer;
  i: Integer;
begin
  lbArrayList.Clear;
  lbArrayList.Sorted := False;
  range := edtRange.Value;
  elementCount := edtArraySize.Value;
  Randomize;
  for i:= 1 to elementCount do
    lbArrayList.Items.Add(IntToStr(Random(range)));
end;

procedure TfrmMain.acMoveUpExecute(Sender: TObject);
var
  CurrIndex: Integer;
begin
  with lbArrayList do
    if ItemIndex > 0 then
    begin
      CurrIndex := ItemIndex;
      Items.Move(ItemIndex, (CurrIndex - 1));
      ItemIndex := CurrIndex - 1;
    end;
end;

procedure TfrmMain.acMoveDownExecute(Sender: TObject);
var
  CurrIndex, LastIndex: Integer;
begin
  with lbArrayList do
  begin
    CurrIndex := ItemIndex;
    LastIndex := Items.Count;
    if ItemIndex <> -1 then
    begin
      if CurrIndex + 1 < LastIndex then
      begin
        Items.Move(ItemIndex, (CurrIndex + 1));
        ItemIndex := CurrIndex + 1;
      end;
    end;
  end;
end;

procedure TfrmMain.acMoveUpUpdate(Sender: TObject);
begin
  acMoveUp.Enabled := (lbArrayList.ItemIndex <> -1) and (lbArrayList.ItemIndex <> 0 ) ;
end;

procedure TfrmMain.acMoveDownUpdate(Sender: TObject);
begin
  acMoveDown.Enabled := (lbArrayList.ItemIndex <> -1) and (lbArrayList.ItemIndex < lbArrayList.Items.Count-1) ;
end;

procedure TfrmMain.acClearExecute(Sender: TObject);
begin
  lbArrayList.Items.Clear;
end;

procedure TfrmMain.acClearUpdate(Sender: TObject);
begin
  acClear.Enabled := lbArrayList.Items.Count > 0;
end;

procedure TfrmMain.acRemoveExecute(Sender: TObject);
begin
  if lbArrayList.ItemIndex>0 then
    lbArrayList.Items.Delete(lbArrayList.ItemIndex);
end;

procedure TfrmMain.acRemoveUpdate(Sender: TObject);
begin
  acRemove.Enabled := lbArrayList.ItemIndex>0;
end;

//1. listbox.items to array
//2. sorting/searching in array
//3. generate output xml

procedure TfrmMain.btnSearchClick(Sender: TObject);
var
  inputArray: Array of Integer;
  i, elementToFind, resultIndex: Integer;
  tmpMemo: TMemo;
  xsl, html: WideString;
  fs: TFileStream;
  ss : TStringStream;
begin
  if lbArrayList.Items.Count = 0 then
    begin
      MessageDlg('Input array is empty.', mtWarning, [mbOK], 0);
      Exit;
    end;

  if Trim(edtSearchValue.Text) = EmptyStr then
    begin
      MessageDlg('Incorrect value.', mtWarning, [mbOK], 0);
      edtSearchValue.SetFocus;
      Exit;
    end;

  if TryStrToInt(Trim(edtSearchValue.Text), elementToFind) = False then
    begin
      MessageDlg('Incorrect value.', mtWarning, [mbOK], 0);
      edtSearchValue.SetFocus;
      Exit;
    end;

  //1. listbox to array
  try
    SetLength(inputArray, lbArrayList.Items.Count);
    for i:= 0 to lbArrayList.Items.Count-1 do
      inputArray[i] := StrToInt(lbArrayList.Items[i]);
  except
    on E:Exception do
      begin
        MessageDlg(E.Message, mtError, [mbOK],0);
        Exit;
      end;
  end;

  //2. sorting and searching in array
  try
    Common.BubbleSort(inputArray);
    resultIndex := Common.BinarySearch(inputArray,elementToFind);
  except
    on E:Exception do
      begin
        MessageDlg(E.Message, mtError, [mbOK],0);
        Exit;
      end;
  end;

  //generate output xml
  try
    tmpMemo := TMemo.Create(nil);
    tmpMemo.Parent := Self;
    tmpMemo.Visible := false;
    try
      tmpMemo.Lines.Add('<?xml version="1.0" encoding="UTF-8"?>');
      tmpMemo.Lines.Add('<document>');
      tmpMemo.Lines.Add('<arrays>');
      tmpMemo.Lines.Add('<array id = "IN_ARRAY">');
      for i:=0 to lbArrayList.Items.Count-1 do
        begin
         tmpMemo.Lines.Add('<element>');
         tmpMemo.Lines.Add(Format('<index>%d</index>', [i]));
         tmpMemo.Lines.Add(Format('<value>%s</value>', [lbArrayList.Items[i]]));
         tmpMemo.Lines.Add('</element>');
        end;
      tmpMemo.Lines.Add('</array>');
      tmpMemo.Lines.Add('<array id = "SORT_ARRAY">');
      for i:=0 to Length(inputArray)-1 do
        begin
         tmpMemo.Lines.Add('<element>');
         tmpMemo.Lines.Add(Format('<index>%d</index>', [i]));
         tmpMemo.Lines.Add(Format('<value>%d</value>', [inputArray[i]]));
         tmpMemo.Lines.Add('</element>');
        end;
      tmpMemo.Lines.Add('</array>');
      tmpMemo.Lines.Add('</arrays>');
      tmpMemo.Lines.Add(Format('<input_value>%d</input_value>', [elementToFind]));
      tmpMemo.Lines.Add(Format('<result>%d</result>', [resultIndex]));
      tmpMemo.Lines.Add('</document>');

      tmpMemo.Lines.SaveToFile(ExtractFilePath(Application.ExeName)+'\BinarySearch.xml');

      memoXML.Lines.Clear;
      memoXML.Lines.Text := tmpMemo.Lines.Text;
    finally
      tmpMemo.Free;
    end;

    //Check generated XML
    if not XMLDoc.loadXML(memoXML.Lines.Text) then
      with XMLDoc.parseError do
        begin
          MessageDlg(Reason + ' at ' + IntToStr(Line) + ',' +
            IntToStr(LinePos), mtError, [mbOK], 0);
          Exit;
        end;
    PageControl1.ActivePage := tsXML;
  except
    on E:Exception do
      begin
        MessageDlg(E.Message, mtError, [mbOK],0);
        Exit;
      end;
  end;
end;

procedure TfrmMain.btnXSLTClick(Sender: TObject);
begin
  OpenDialog.Filter := XSLTFilter;
  if OpenDialog.Execute then
    begin
      edtXSLT.Text := OpenDialog.Filename;
      memoXSLT.Lines.Clear;
      memoXSLT.Lines.LoadFromFile(edtXSLT.Text);
      if not XSLTDoc.load(edtXSLT.Text) then
        with XSLTDoc.parseError do
          begin
            MessageDlg(Reason + ' at ' + IntToStr(Line) + ',' +
              IntToStr(LinePos), mtError, [mbOK], 0);
            Exit;
          end;
        PageControl1.ActivePage := tsXSLT;
    end
end;

procedure TfrmMain.btnViewInBrowserClick(Sender: TObject);
var
  outputXML: IXMLDOMDocument;
  HTMLDocument: IHTMLDocument2;
begin
{
  if edtXSLT.Text = EmptyStr then
    begin
      MessageDlg('Please select XSLT file.', mtWarning, [mbOK], 0);
      Exit;
    end;

  if not(FileExists(edtXSLT.Text)) then
    begin
      MessageDlg(Format('File %s doesn''t not exists.', [edtXSLT.Text]), mtWarning, [mbOK], 0);
      Exit;
    end;
}
  //
  if not XSLTDoc.loadXML(memoXSLT.Lines.Text) then
    with XSLTDoc.parseError do
      begin
        MessageDlg(Reason + ' at ' + IntToStr(Line) + ',' +
          IntToStr(LinePos), mtError, [mbOK], 0);
        Exit;
      end;
  if not XMLDoc.loadXML(memoXML.Lines.Text) then
    with XMLDoc.parseError do
      begin
        MessageDlg(Reason + ' at ' + IntToStr(Line) + ',' +
             IntToStr(LinePos), mtError, [mbOK], 0);
        Exit;
      end;
  try
    outputXML :=CoDOMDocument.Create;
    XMLDoc.transformNodeToObject(XSLTDoc, outputXML);
    HTMLDocument := (wbBrowser.Document as IHTMLDocument2);
    HTMLDocument.body.innerHTML := outputXML.xml;
    PageControl1.ActivePage := tsWebBrowser;
    outputXML := nil;
  except
    on E:Exception do
      begin
        MessageDlg(E.Message, mtError, [mbOK],0);
        Exit;
      end;
  end;
end;

end.
